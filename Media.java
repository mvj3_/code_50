// Play audio or video from a file within the application
// Put the media file into the res/raw folder of your application
MediaPlayer mp = MediaPlayer.create(this, R.raw.yourSoundId);
mp.start();	

// Play audio or video given a path to a file or a URL
MediaPlayer mp = new MediaPlayer();
mp.setDataSource(FILE_PATH_OR_URL);
mp.prepare();
mp.start();	

// Start recording audio
// To use these MediaRecorder methods, AndroidManifest.xml must have the following permission:
// <uses-permission android:name="android.permission.RECORD_AUDIO"/>
MediaRecorder recorder = new MediaRecorder();
recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
recorder.setOutputFile(PATH_NAME); // The file must already exist
recorder.prepare();
recorder.start();

// Stop recording audio
recorder.stop();
recorder.release();